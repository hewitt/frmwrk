# Install Git Worktree

This allows multiple folders per Git connection. Having each branch in specific places helps to keep track of where you are from a visual perspective.

```
git init
git remote add -t \* -f origin https://oauth2:{{PERSONAL_TOKEN}}@gitlab.com/hewitt/frmwrk.git
git fetch --all
git worktree add --track -b development development origin/development
git worktree add --track -b main main origin/main
```
