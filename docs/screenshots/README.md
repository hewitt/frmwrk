# docs/screenshots

This is where screenshots of your application can be placed. Screenshots are useful to keep track of changes, training, feature proposals, personal portfolios, and more. It's always a good idea to take screenshots of each major release of the application and keep them here. In addition to keeping screenshots safe in the docs folder, source control allows you to look back in time on specific parts of the site. If you consistently keep the same names for screenshots of the same page/subject, then you will be able to see how those pages/subjects change over time.
