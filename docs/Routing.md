# Routing

https://github.com/steampixel/simplePHPRouter

## Controllers

Use controllers to easily select the proper method to fire for each endpoint. This way we can keep the code for that route in its own separate file away from the rest of the routing logic.

```
Route::add('/exampleone', 'Src\Controller\HomeController::exampleOne');
```

## GET Routes

*Optional*

This will only run if it detects a GET request. Any POST requests sent to this route will result in a 404 error. This prevents mixups in logic. Having a special end-point for GETs allows the code to be more properly separated and understandable.

```
Route::add('/contact-form', function(){
    echo "<form method='post'><input type='text' name='test' /><input type='submit' value='send' /></form>";
},'get');
```

## POST Routes

*Optional*

This will only run if it detects a POST request. Any GET requests sent to this route will result in a 404 error. This is a great way to create an end-point that can't be easily seen/used by the end user. This is also great for API end-points.

```
Route::add('/contact-form', function(){
    echo 'Hey! The form has been sent:<br/>';
    print_r($_POST);
},'post');
```

## RegEx Parameters

*Optional*

Using a RegEx, you can require a URL to contain a specific set of characters as variables within a URL. This allows us to accept variables but keep the URL logically structured for humans.

```
Route::add('/item/([0-9]*)/edit', function($var1){
    echo $var1.' is a great number!';
});
```

## Protecting Pages with Login

*Optional*

Sometimes we want to prevent just anybody from accessing pages, or maybe some pages require a user account to associate things with an individual; in these cases, we need to "protect" pages by placing them behind our login feature.

To begin, create a file named "protected.php" in the "app/src/routes/" folder. In this file, you can insert routes you wish to protect by using the following code:

```
Route::protect('/movies');
```

Note that any routes you protect will not only protect the page you specified, but any subpages, as well. If you protect "/movies", it will protect "/movies/add" as well. While the protection extends to children, it does *not* equate to a wildcard. "/movies" will be protected and require login, but "/moviesnew" will *not* be protected.

### Protect ALL THE THING!

By using the following code, you lock literally every single page behind the user login. The "/login" page is the only page that cannot be put behind the user login for obvious reasons.

```
Route::protect('');
```
