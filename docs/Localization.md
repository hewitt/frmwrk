# Localization

Automatic replacement of text to allow for quick and easy translation into multiple languages. Each language is to be separated into its own folder, split among roles for each folder (e.g. titles, text, labels, etc.). In order to translate to a new language, a translation service will take the files, translate, and provide the files back to the developer; after validating and uploading the files, simply changing the user's language preference will change all text on the site to that language. If a translation slug is used but is not defined in the language file of the selected language, the system will attempt to find the translation slug in the English language file. If the slug is not found in the English language file either, the system will print the slug on the page. Anytime you see a translation slug on the website, it's because the translation string does not exist in the language files; either the slug was misspelled, or the translation string just doesn't exist, yet.



# Defintions:

- ***Language file***: PHP file for a single role (e.g. titles, labels, links, etc.) with one element: an array containing all translation strings for that role. Language files are named after their role (e.g. "titles.php"), and go inside a folder named after the language they represent (e.g. "en-US", "fr-CA", "de-DE", etc.).

- ***Role***: A specific category a translation string belongs to. This is used to logically separate translation strings in a way that a human can understand and easily locate new strings under. For example, a title string named "login" might contain different text than a link named "login", and their translation strings might be vastly different. To separate these two (or more) strings based on their usecase, we store the two translation strings in separate files each dedicated to their specific usecase category.

- ***Translation slug***: Text that selects the role and translation string from the translation file. Separated by a period. E.g. "title.welcome". This is what goes inside the calling function; e.g. __('title.welcome').

- ***Translation string***: String in the localized file that the code inserts onto a page. Translation strings reside in their language file under "app/lang/".



# Install

To install the localization plugin, you need to copy over a few files, and insert a bit of text into the "/public/index.php" file.

## Files to copy over

```
app/src/core/localization.php
app/src/functions/localization.php
app/src/lang/*
```

## Public Index Update

Under the "File Includes" section, load the localization functions file. This includes the "__('')" calling function that allows short-code translations.

```
require SRC . '/functions/localization.php';
```

**After** the "User Object" section, insert the following block of code. It's important that you place this *after* the "User Object" section, as the user object sets the language based on the user's preference.

```
/*
|--------------------------------------------------------------------------
| Localization
|--------------------------------------------------------------------------
|
| People speak different languages. This allows users to change the site's
| language for better understanding and customization.
|
*/
use Src\Core\Localization;
$localization = new Localization();

//change site's localization to the user's selected language
if (isset($user)) $localization->language = $user->language;
```



# Uninstall

1. Replace localization references in view pages
2. Remove the localization function include from the "File Includes" section in /public/index.php
3. Remove the "Localization" section in /public/index.php
4. Delete the following files:
    - /app/src/core/localization.php
    - /app/src/functions/localization.php
    - /app/src/lang/



# Basic Translation

When we have a word that isn't universal, users may prefer to have the website shown to them in a language that is comfortable for them. By adding translation slugs to the site's view pages, we can have the system replace those slugs with actual words that are appropriate to the context they are displayed in.

```
app/lang/en-US/links.php:

<?php
return [
    'login' => 'Click to login',
];
```

### Output:

```
__('link.login') // "Click to login"
```



# Counting

By putting a number (int/decimal) as a second parameter, the system recognizes that it is meant to be a count. If the translation string has ":count" anywhere in the string (even multiple times), each instance of ":count" will be replaced with the number itself.

```
app/lang/en-US/text.php:

<?php
return [
    'orange_count' => 'Oranges: :count',
];
```

### Output:

```
__('text.orange_count', 5) // "Oranges: 5"
```



# Multiple Translations

Translation strings can contain multiple translations. Starting at zero, you can use the second parameter to select from the array of options in a translation string.

```
app/lang/en-US/text.php:

<?php
return [
    'colors' => 'Red|Green|Blue',
];
```

### Output:

```
__('text.colors', 0) // "Red"
```



# Count-based Multiple Translations

Allows the definition of multiple translations depending on pluralization. For example, in English, you would say "1 Item in Cart", but if there ws more than 1, it would be grammatically incorrect to say "2 Item in Cart". You would then switch to saying "2 Items in Cart". Multiples allows the use of different phrases based on the value passed to it. This only works based on English rules of pluralization: a count of 1 results in the first part of the string being used, anything else (0, or 2+) uses the second part of the string. Both parts are separated by the pipe "|" symbol.

```
app/lang/en-US/navigation.php:

<?php
return [
    'item_in_cart' => ':count Item in Cart|:count Items in Cart',
];
```

### Output:

```
__('navigation.item_in_cart', 1) // "1 Item in cart"

__('navigation.item_in_cart', 5) // "5 Items in cart"
```



# Replace String Segments

If you wish to have a variable in the middle of your string (for example, a href in a link), you can provide the __() function with an array in the second argument. The array MUST consist of a key and a value; the key MUST be identical to the key you use in the translation string.

```
app/lang/en-US/titles.php:

<?php
return [
    'welcome_user' => 'Welcome, :user! Happy :day!',
];
```

### Output:

```
__('titles.welcome_user', [':user'=>'Mark', ':day'=>'Wednesday']) // "Welcome, Mark! Happy Wednesday!"
```