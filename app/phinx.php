<?php

require_once 'src/Functions/General.php';

$config =
[
    'paths' => [
        'migrations' => '%%PHINX_CONFIG_DIR%%/database/migrations',
        'seeds' => '%%PHINX_CONFIG_DIR%%/database/seeds'
    ],
    'environments' => [
        'default_migration_table' => 'migrations',
        'default_environment' => 'default',
        'default' => [
            "adapter" => setEnv('DB_TYPE', 'mysql'),
            "host" => setEnv('DB_HOST', 'localhost'),
            "name" => setEnv('DB_NAME', 'frmwrk-local'),
            "user" => setEnv('DB_USER', 'root'),
            "pass" => setEnv('DB_PASS', ''),
            "port" => setEnv('DB_PORT', '3306')
        ]
    ],
    'version_order' => 'creation'
];

foreach ($_SERVER as $key => $value)
{
    if (strpos($key, 'PHINX_DB_') === 0)
    {
        $environment_name = str_replace('PHINX_DB_', '', $key);
        $config['environments'][$environment_name] = (array)json_decode($value);
    }
}

return $config;
