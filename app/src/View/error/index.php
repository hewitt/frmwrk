<div class='container'>
    <h1>Error</h1>
    <p>This is the Error-page. Will be shown when a page (= controller / method) does not exist.</p>
    <p><a href='<?=URL?>'>Go home</a></p>
</div>
