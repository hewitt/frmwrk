<section id='login'>
    <a href='<?=URL?>'><img src='<?=URL?>/images/manifest-icons/launcher-icon-3x.png' alt='Site logo'></a>

    <form method='post'>
        <label>
            <strong><?=__('labels.email_address')?></strong>
            <input type='email' name='email' maxlength='255' required>
        </label>

        <label>
            <strong><?=__('labels.password')?></strong>
            <input type='password' name='password' maxlength='1000' required>
        </label>

        <button class='primary full'><?=__('labels.login')?></button>
    </form>

    <p><a href='<?=URL?>/register'><?=__('links.create_account')?></a></p>

    <p><a href='<?=URL?>/reset'><?=__('links.forgot_password')?></a></p>
</section>
