<section id='login'>
    <a href='<?=URL?>'><img src='<?=URL?>/images/manifest-icons/launcher-icon-3x.png' alt='Site logo'></a>

    <h1><?=__('titles.delete_account')?></h1>

    <p><?=__('text.are_you_sure')?></p>

    <p><?=__('text.delete_account_description')?></p>

    <form method='post'>
        <label>
            <strong><?=__('labels.email_address')?></strong>
            <input type='email' name='email' maxlength='255' required value='m@mh1.co'>
        </label>

        <button class='full bg-red white'><?=__('labels.yes_delete')?></button>
    </form>

    <p><a class='btn full' href='<?=URL?>/account'><?=__('links.no_delete')?></a></p>
</section>
