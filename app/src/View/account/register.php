<section id='login'>
    <a href='<?=URL?>'><img src='<?=URL?>/images/manifest-icons/launcher-icon-3x.png' alt='Site logo'></a>

    <h1><?=__('titles.create_account')?></h1>

    <p><?=__('text.registration_description')?></p>

    <form method='post'>
        <label>
            <strong><?=__('labels.email_address')?></strong>
            <input type='email' name='email' maxlength='255' required>
        </label>

        <label>
            <strong><?=__('labels.password')?></strong>
            <input type='password' name='password' maxlength='1000' required>
        </label>

        <button class='primary full'><?=__('labels.register')?></button>
    </form>

    <p><?=__('text.registration_agreement')?></p>

    <div class='login-link-box'><?=__('links.register_login_box')?></div>
</section>
