<section id='login'>
    <a href='<?=URL?>'><img src='<?=URL?>/images/manifest-icons/launcher-icon-3x.png' alt='Site logo'></a>

    <h1><?=__('titles.password_reset')?></h1>

    <p><?=__('text.password_reset_request_description')?></p>

    <form method='post'>
        <label>
            <strong><?=__('labels.email_address')?></strong>
            <input type='email' name='email' maxlength='255' required>
        </label>

        <button class='primary full'><?=__('labels.reset_password')?></button>
    </form>

    <div class='login-link-box'>
        <?=__('links.reset_login_box')?>
    </div>
</section>
