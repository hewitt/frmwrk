<section class='container'>
    <h1><?=__('titles.account')?></h1>

    <p>
        <strong><?=__('labels.email_address')?></strong><br>
        <?=$user->email?>
    </p>

    <p>
        <strong><?=__('labels.language')?></strong><br>
        <?=$user->language?>
    </p>

    <p>
        <strong><?=__('labels.date_registered')?></strong><br>
        <?=date(TIME_FORMAT, $user->date_registered)?>
    </p>

    <p>
        <strong><?=__('labels.actions')?></strong>
        <ul>
            <li><a href='<?=URL?>/logout'><?=__('links.logout')?></a></li>
            <li><a href='<?=URL?>/reset'><?=__('links.change_password')?></a></li>
            <li><a href='<?=URL?>/account/delete' class='red'><?=__('links.delete_account')?></a></li>
        </ul>
    </p>
</section>
