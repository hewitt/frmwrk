<section class='container'<?=(!$user->is_logged_in() ? " id='login'" : null)?>>
    <?php
        if (!$user->is_logged_in())
        {
            echo "<a href='" . URL . "'>";
                echo "<img src='" . URL . "/images/manifest-icons/launcher-icon-3x.png' alt='Site logo'>";
            echo "</a>";
        }
    ?>

    <h1><?=__('titles.password_reset')?></h1>

    <p><?=__('text.password_reset_request_description')?></p>

    <p><?=__('text.password_requirements')?></p>

    <ul>
        <li><?=__('text.password_requirements_1')?></li>
        <li><?=__('text.password_requirements_2')?></li>
        <li><?=__('text.password_requirements_3')?></li>
    </ul>
    <hr>

    <form method='post'>
        <label>
            <strong><?=__('labels.new_password')?></strong>
            <input type='password' name='password' maxlength='1000' required>
        </label>

        <button class='primary<?=(!$user->is_logged_in() ? ' full' : null)?>'><?=__('labels.reset_password')?></button>
    </form>

    <?php
        if (!$user->is_logged_in())
        {
            echo "<div class='login-link-box'>" . __('links.reset_login_box') . "</div>";
        }
    ?>
</section>
