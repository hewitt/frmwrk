<!doctype html>
<html lang='en'>
    <head>
        <meta charset='utf-8'>
        <meta http-equiv='X-UA-Compatible' content='IE=edge'>
        <meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=yes'>

        <title><?=(isset($page_title) ? $page_title . ' | ' : null)?>Frmwrk</title>

        <meta name='description' content='Modern PHP framework to base future projects off of'>
        <meta name='author' content='Mark Hewitt www.mh1.co'>
        <meta name='og:locale' content='en_US'>
        <meta name='theme-color' content='#ffffff'>
		<meta name='robot' content='noindex, nofollow' />

		<!-- Add to home screen for Safari on iOS -->
		<meta name='apple-mobile-web-app-capable' content='yes'>
		<meta name='apple-mobile-web-app-status-bar-style' content='black'>
		<meta name='apple-mobile-web-app-title' content='Reader'>
		<link rel='apple-touch-icon' href='<?=URL?>/images/manifest-icons/launcher-icon-3x.png'>
		<meta name='msapplication-TileImage' content='<?=URL?>/images/manifest-icons/launcher-icon-3x.png'>
		<meta name='msapplication-TileColor' content='#d74a38'>

        <link rel='icon' type='image/png' href='<?=URL?>/images/manifest-icons/launcher-icon-1x.png' sizes='32x32'>
		<link rel='manifest' href='<?=URL?>/manifest.json'>
        <?php
            if (isset($stylesheets))
            {
                foreach ($stylesheets as $stylesheet)
                {
                    echo "<link href='{$stylesheet}?" . SITE_VERSION . "' rel='stylesheet'>\n";
                }
            }
        ?>

        <!-- CSS -->
        <link rel='stylesheet' href='https://fonts.googleapis.com/css2?family=DM+Mono:wght@300&family=Nunito:wght@300;400;700;900&display=swap'>
        <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.0/css/all.min.css'>
        <link href='<?=URL?>/css/general.min.css' rel='stylesheet'>
    </head>
    <body>
        <?php
            if (isset($_SESSION['toast']))
            {
                $toasts = json_decode($_SESSION['toast']);

                echo "<article class='toast {$toasts[0]->status}' title='" . __('text.click_to_hide_toast') . "'>";
                    echo "<section>";
                        echo "<i id='toast-icon' class='fas fa-" . ($toasts[0]->status == 'error' ? 'exclamation-triangle' : 'thumbs-up') . " fa-2x'></i>";
                        echo "<div>";
                            echo "<b class='title'>" . (isset($toasts[0]->title) ? $toasts[0]->title : ucwords($toasts[0]->status) . ':') . "</b><br>";
                            echo "<div class='content'>";
                                $toast_messages = [];
                                foreach ($toasts as $toast) $toast_messages[] = $toast->message;
                                echo implode('<br>', $toast_messages);
                            echo "</div>";
                        echo "</div>";
                        echo "<i class='fas fa-times close'></i>";
                    echo "</section>";
                echo "</article>";

                //clear $_SESSION['toast'] so we stop seeing the message after it's been displayed
                unset($_SESSION['toast']);
            }
        ?>
