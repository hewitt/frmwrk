<nav class='navigation'>
    <a href='<?=URL?>'><?=__('navigation.home')?></a>
    <?php
        if ($user->is_logged_in())
        {
            echo "<a href='" . URL . "/account'>" . __('navigation.account') . "</a>";
        }
    ?>
</nav>
