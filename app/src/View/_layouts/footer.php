
        <!-- define the project's URL (to make AJAX calls possible, even when using this in sub-folders etc) -->
        <script>var URL = '<?=URL?>';</script>

        <!-- JavaScript -->
        <script src='https://code.jquery.com/jquery-3.6.0.min.js' integrity='sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=' crossorigin='anonymous'></script>
        <script src='<?=URL?>/js/general.min.js'></script>

        <?php
            if (isset($js_scripts))
            {
                foreach ($js_scripts as $script)
                {
                    echo "<script src='{$script}?" . SITE_VERSION . "'></script>\n";
                }
            }
        ?>
    </body>
</html>
