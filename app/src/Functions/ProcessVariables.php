<?php
/*
 * Some numbers are integers, but have the ability to be decimals. We want to trim the numbers to as short as they'll go,
 * while still having the option to have empty decimals, should the value we're trimming be a price or something.
 */
function trim_decimals ($value, $minimum_decimals = 0)
{
    $integer = floor($value);
    $decimals = round($value - $integer, $minimum_decimals);

    if ($decimals > 0)
    {
        $decimals = rtrim($decimals, '.0');
    }

    $value = (float)$integer + (float)$decimals;

    return $value;
}

function format_decimals ($value, $minimum_decimals = 0)
{
    $value = trim_decimals($value, $minimum_decimals);
    $value = number_format($value, $minimum_decimals);

    return $value;
}

function processString ($input)
{
    //let's clean it up, then tell PHP to treat it like a string
    $output = (string)trim($input);

    //we need to make sure it's in the proper format before we begin, as PHP doesn't like mismatched formats
    $output = iconv(mb_detect_encoding($input, mb_detect_order(), true), 'UTF-8', $output);

    //we know there are a few bad characters out there, and we need to get rid of them from the string
    $weird_characters =
    [
        '' => '-',
        '”' => '"'
    ];
    $output = str_replace(array_keys($weird_characters), array_values($weird_characters), $output);

    return $output;
}

function processInt ($input, $length = 8)
{
    //convert it to a string, trim it, clean it up, make sure it's prim and proper before messing with it
    $output = processString($input);

    //let's get rid of all non-numbers in the string
    $output = preg_replace( '/\D/', '', html_entity_decode($output));

    //almost always, we want there to be a length, but sometimes we just want all the digits
    if ($length !== false) $output = substr($output, 0, $length);

    //return the value as a proper int
    return intval($output);
}

function processFloat ($input, $decimals = 4)
{
    //convert it to a string, trim it, clean it up, make sure it's prim and proper before messing with it
    $output = processString($input);

    //let's get its float value, and then round it to the proper decimal place
    $output = round(floatval($output), $decimals);
    $output = trim_decimals($output, $decimals);

    return $output;
}

function format_price ($amount, $unit = null)
{
    //we don't want negative numbers
    //leave the "$0 = on request" functionality to display_requestable_price()
    if ($amount < 0) $amount = 0;

    return '$' . format_decimals($amount, 2);
}
