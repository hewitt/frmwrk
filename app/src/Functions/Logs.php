<?php

use Src\Model\Logs;

function log_emergency ($message)
{
    $logs = new Logs();
    return $logs->emergency($message);
}
function log_alert ($message)
{
    $logs = new Logs();
    return $logs->alert($message);
}
function log_critical ($message)
{
    $logs = new Logs();
    return $logs->critical($message);
}
function log_error ($message)
{
    $logs = new Logs();
    return $logs->error($message);
}
function log_warning ($message)
{
    $logs = new Logs();
    return $logs->warning($message);
}
function log_notice ($message)
{
    $logs = new Logs();
    return $logs->notice($message);
}
function log_info ($message)
{
    $logs = new Logs();
    return $logs->info($message);
}
function log_debug ($message)
{
    $logs = new Logs();
    return $logs->debug($message);
}
