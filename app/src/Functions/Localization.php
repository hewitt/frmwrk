<?php

//localization: translate function
function __()
{
    global $localization;
    $args = func_get_args();
    return $localization->getTranslation(func_get_args());
}
