<?php

//print input with pre tags surrounding it, making it legible in the browser
//pa stands for "print all"
function pa ($array)
{
    echo "<pre>";
        print_r($array);
    echo "</pre>";
}

function get_ip ()
{
    if (getenv('HTTP_CLIENT_IP') and strcasecmp(getenv('HTTP_CLIENT_IP'), 'unknown'))
    {
        $ip_address = getenv('HTTP_CLIENT_IP');
    }
    elseif (getenv('HTTP_X_FORWARDED_FOR') and strcasecmp(getenv('HTTP_X_FORWARDED_FOR'), 'unknown'))
    {
        $ip_address = getenv('HTTP_X_FORWARDED_FOR');
    }
    elseif (getenv('REMOTE_ADDR') and strcasecmp(getenv('REMOTE_ADDR'), 'unknown'))
    {
        $ip_address = getenv('REMOTE_ADDR');
    }
    elseif (isset($_SERVER['REMOTE_ADDR']) and $_SERVER['REMOTE_ADDR'] and strcasecmp($_SERVER['REMOTE_ADDR'], 'unknown'))
    {
        $ip_address = $_SERVER['REMOTE_ADDR'];
    }
    else
    {
        $ip_address = 'Unknown';
    }

    return $ip_address;
}

//popup messages
function toast ($message, $status = 'success')
{
    //if toast session variable is already set, decode that into an array, so we can add more
    $toasts = (isset($_SESSION['toast']) ? json_decode($_SESSION['toast']) : []);

    //add the current message and status to the list
    $toasts[] =
    [
        'message' => $message,
        'status' => $status
    ];
    $_SESSION['toast'] = json_encode($toasts);

    return true;
}

function redirect ($url = null)
{
    $url = URL . str_replace(URL, '', $url);
    header('Location: ' . $url);
    exit;
}

function setenv ($name, $default = null)
{
    if (isset($_SERVER[$name]))
    {
        return $_SERVER[$name];
    }
    elseif (isset($_ENV[$name]))
    {
        $_SERVER[$name] = $_ENV[$name];
        return $_SERVER[$name];
    }
    elseif (!empty($default))
    {
        $_SERVER[$name] = $default;
        return $_SERVER[$name];
    }

    return null;
}
