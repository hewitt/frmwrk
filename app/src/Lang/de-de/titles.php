<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Page Titles
    |--------------------------------------------------------------------------
    |
    | The following language lines contain text for page meta tites, and H2
    | titles embeded on each page.
    |
    */

    'welcome' => 'Willkommen zu frmwrk!',
    'account' => 'Kontoinformationen',
];
