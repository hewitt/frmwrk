<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Table Headers
    |--------------------------------------------------------------------------
    |
    | Localization for table column headers. This is mostly shown through
    | Javascript, so we need to use these in the PHP scripts that send data to the
    | client.
    |
    */

    'quote' => 'Quote|Quotes',
];
