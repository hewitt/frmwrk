<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Text
    |--------------------------------------------------------------------------
    |
    | The following language lines contain text for page meta tites, and H2
    | titles embeded on each page.
    |
    */

    'homepage' => "This is the homepage",
    'login_to_start' => "Please <a href=':link'>login</a> to get started.",
    'registration_description' => "You must create an account before you can begin using the site.",
    'registration_agreement' => "By continuing, you agree to our <a href=':URL/terms' target='_blank'>Terms and Conditions</a> and our <a href=':URL/privacy' target='_blank'>Privacy Policy</a>",
    'password_reset_request_description' => "Enter your email to reset your password. You will receive an email with a link to a page that will let you enter a new password.",
    'password_reset_form_description' => "Change your password if you forgot it, or if there's a possibility that someone else might know it.",
    'password_requirements' => 'Password requirements:',
    'password_requirements_1' => "Passwords must be at least 6 characters long, but we strongly recommend a minimum of 12 characters. Longer passwords are exponentially more secure.",
    'password_requirements_2' => "Special characters, numbers, etc. are not required, but are suggested. The length of a password is far more important than the password itself.",
    'password_requirements_3' => "DO NOT use a password you've used on other websites/services.",
    'are_you_sure' => "Are you absolutely sure? This cannot be undone.",
    'delete_account_description' => "To confirm you want to delete your account, enter your email address in the field below.",
];
