<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Links and buttons
    |--------------------------------------------------------------------------
    |
    | Localization for links and buttons across the site. As links can be
    | styled to look like buttons, they are included in this document.
    |
    */

    'create_account' => 'Create a new account',
    'forgot_password' => 'Forgot your password?',
    'register_login_box' => "Already have an account? <a href=':URL/login'>Login</a>",
    'reset_login_box' => "Do you remember your password? <a href=':URL/login'>Login</a>",
    'logout' => 'Logout',
    'change_password' => 'Change Password',
    'delete_account' => 'Delete Account',
    'no_delete' => "No, go back to my account profile",
];
