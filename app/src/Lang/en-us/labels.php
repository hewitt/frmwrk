<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Labels
    |--------------------------------------------------------------------------
    |
    | Localization for form labels.
    |
    */

    'email_address' => 'Email Address:',
    'password' => 'Password:',
    'new_password' => 'New Password:',
    'language' => 'Language:',
    'date_registered' => 'Date Registered:',
    'actions' => 'Actions:',
    'login' => 'Login',
    'register' => 'Register',
    'reset_password' => 'Reset Password',
    'yes_delete' => "Yes, delete my account",
];
