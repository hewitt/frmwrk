<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Page Titles
    |--------------------------------------------------------------------------
    |
    | The following language lines contain text for page meta tites, and H2
    | titles embeded on each page.
    |
    */

    'welcome' => 'Welcome!',
    'login' => 'Login',
    'register' => 'Register',
    'create_account' => 'Create an Account',
    'terms_and_conditions' => 'Terms &amp; Conditions',
    'privacy_policy' => 'Privacy Policy',
    'reset_password' => 'Reset Password',
    'password_reset' => 'Password Reset',
    'account' => 'User Account',
    'delete_account' => 'Delete Account',
];
