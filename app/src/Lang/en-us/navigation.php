<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Navigation
    |--------------------------------------------------------------------------
    |
    | The following language lines contain text for page meta tites, and H2
    | titles embeded on each page.
    |
    */

    'home' => 'Home',
    'account' => 'Account',
];
