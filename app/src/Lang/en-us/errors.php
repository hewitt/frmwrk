<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Errors
    |--------------------------------------------------------------------------
    |
    | The following language lines contain text for page meta tites, and H2
    | titles embeded on each page.
    |
    */

    'general_access' => "You don't have permission to access that page",
    'registration_fields_required' => 'All fields are required, unless otherwise stated',
    'registration_existing_email' => "That email is already in use. If you already have an account, or are unsure, try <a href=':link'>resetting your password</a>.",
    'registration_password_mismatch' => "Something went wrong when we encrypted your password, and we couldn't finalize your registration. Please try again, or contact us if you see this error again.",
    'registration_login_failed' => "We couldn't log you in after registering your account. Unfortunately, you'll have to do that manually. If you run into any problems, please contact us immediately.",
    'registration_failed' => 'Registration failed. Please try again, or contact us if you see this error again.',
    'password_reset_failure' => "Password couldn't be properly set",
    'password_reset_incorrect_email' => "Please provide a valid email address",
    'password_reset_failure_empty' => 'Please fill out all fields',
    'password_reset_failure_length' => 'Your password must be at least 6 characters long',
    '404_title' => "Sorry, this page isn't available",
    '404_description' => "The page you were looking for couldn't be found.",
];
