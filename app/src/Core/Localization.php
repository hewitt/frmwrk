<?php

namespace Src\Core;

class Localization
{
    public $language = 'en-US';
    protected $files_loaded = [];
    protected $words = [];

    function __construct()
    {
        //load default language from config file
        $this->language = DEFAULT_LANGUAGE;
    }

    public function getTranslation () : string
    {
        $args = func_get_args()[0];
        $key = $args[0];

        /*
         * DEBUGGING
         * 
         * Uncomment the return below in order to see every instance of localized
         * text across the site. This is really useful for tracking down spots that
         * aren't properly using localized text. We want this site to eventually be
         * translatable, and localization is exactly how we'll get there, so we
         * need to use it, or we won't have it available in the future.
         */
        //return $key;

        $key_flattened = str_replace('.', '_', strtolower($key));

        //is `labels_name` string present?
        if (isset($this->words[$key_flattened]))
        //yes
        {
            //translate `labels_name`
            $args[0] = $key_flattened;
            $text = $this->translate($args);
        }
        //no
        else
        {
            //load language's file
            $file_path_parts = explode('.', $key);
            array_pop($file_path_parts);
            $localization_key_prefix = implode('_', $file_path_parts) . '_';
            $file_path = str_replace('\\', '/', SRC . '/Lang/' . $this->language . '/' . implode('/', $file_path_parts) . '.php');

            //does user's language file exist?
            //if we already tried this file, then there's no need in trying it again, now is there?
            if (!in_array($file_path, $this->files_loaded) and file_exists($file_path))
            //yes
            {
                //load file data
                $this->loadLocalizationFile($file_path);
            }
            //no
            else
            {
                //log user's language file is missing

                //does DEFAULT_LANGUAGE file exist?
                $file_path = str_replace('\\', '/', SRC . '/Lang/' . DEFAULT_LANGUAGE . '/' . implode('/', $file_path_parts) . '.php');
                if (file_exists($file_path))
                //yes
                {
                    //load file data
                    $this->loadLocalizationFile($file_path);
                }
                //no
                else
                {
                    //this key might be misspelled, or the language files may have been deleted somehow
                    //log DEFAULT_LANGUAGE file is missing

                    //so that we know it's been attempted, just that it's not loading, so we don't try this all again if it's called again
                    $this->words[$key] = $key;

                    return $key;
                }
            }

            //is `labels_name` string present?
            if (isset($this->words[$key_flattened]))
            //yes
            {
                //translate `labels_name`
                $args[0] = $key_flattened;
                $text = $this->translate($args);
            }
            //no
            else
            {
                //log that this string couldn't be found in user's language, so we can address it later

                //load DEFAULT_LANGUAGE file
                $file_path = str_replace('\\', '/', SRC . '/Lang/' . DEFAULT_LANGUAGE . '/' . implode('/', $file_path_parts) . '.php');
                $this->loadLocalizationFile($file_path);

                //is that string present?
                if (isset($this->words[$key_flattened]))
                //yes
                {
                    //translate `labels_name`
                    $args[0] = $key_flattened;
                    $text = $this->translate($args);
                }
                //no
                else
                {
                    //log that this string couldn't be found in the DEFAULT_LANGUAGE, so we can address it later

                    $this->words[$key_flattened] = $key;
                    return $key;
                }
            }
        }
        //pa($this);

        return $text;
    }

    protected function loadLocalizationFile ($file_path, $force = false) : void
    {
        $localization_path = str_replace('\\', '/', SRC . '/Lang/');

        //does file exist?
        if (file_exists($file_path))
        //yes
        {
            $path_info = pathinfo($file_path);
            $key_flattened = str_replace(['.'.$path_info['extension'], $localization_path], '', $file_path);
            $key_flattened = explode('/', $key_flattened);
            array_shift($key_flattened);
            $localization_key_prefix = implode('_', $key_flattened) . '_';

            //load file data
            $text = include $file_path;
            $this->files_loaded[] = $file_path;

            foreach ($text as $localization_key => $line)
            {
                //if this key isn't already set, set it
                if ($force === true or !isset($this->words[$localization_key_prefix . $localization_key]))
                {
                    $this->words[$localization_key_prefix . $localization_key] = str_replace([':URL', ':url'], URL, $line);
                }
            }
        }
    }

    protected function translate () : string
    {
        $args = func_get_args()[0];
        $key = $args[0];

        if (!isset($this->words[$key])) return $key;
        $text = htmlentities($this->words[$key]);

        //check for replacements and formatting
        if (true or count($args) > 1)
        {
            //get translations
            if (isset($args[1]) and is_array($args[1])) $translations = $args[1];
            elseif (isset($args[1]) and is_numeric($args[1])) $count = intval($args[1]);
            if (isset($args[2]) and is_array($args[2])) $translations = $args[2];

            /*
             * DEBUGGING
             *
             * Add a slash to the last dot in this comment block in order to activate the PHP
             * below, and see information about the translation.
             *
            pa($key);
            if (isset($translations)) pa($translations);
            if (isset($count)) pa("Count: ".$count);
            /* */

            $text_sections = explode('|', $text);

            //if there are 2 or more sections, we need to do special stuff
            if (count($text_sections) == 2)
            {
                //this is a plural, and thus it's [1|*]
                //if there is no value assigned, return the singular
                if (!isset($count) or $count == 1) $text = $text_sections[0];
                else $text = $text_sections[1];
            }
            elseif (count($text_sections) >= 3)
            {
                //loop through each section
                foreach ($text_sections as $section_key => $section)
                {
                    $bounds_text = strstr($section, ' ', true);

                    //does this section have a bound defined?
                    //if there's a bracket, get the bracket, otherwise, set the bounds to 0 for that section
                    if (in_array(substr($section, 0, 1), ['{', '[']))
                    //yes
                    {
                        $section = str_replace($bounds_text.' ', '', $section);

                        $bounds = explode(',', trim($bounds_text, '{}[]'));

                        //if there's only one bound
                        if (count($bounds) == 1)
                        {
                            if ($count == $bounds[0])
                            {
                                //this is the one
                                $text = $section;
                            }
                        }
                        //else if there are two bounds
                        else
                        {
                            $lower_bound = $bounds[0];
                            $upper_bound = $bounds[1];

                            if ($upper_bound == '*')
                            {
                                if ($count >= intval($lower_bound))
                                {
                                    //this is the one
                                    $text = $section;
                                }
                            }
                            else
                            {
                                if ($count >= intval($lower_bound) and $count <= intval($upper_bound))
                                {
                                    //this is the one
                                    $text = $section;
                                }
                            }
                        }
                    }
                    //no
                    else
                    {
                        //is this the last text section?
                        if ($section_key === array_key_last($text_sections))
                        {
                            if ($count >= $section_key)
                            {
                                //this is the one
                                $text = $section;
                            }
                        }
                        else
                        {
                            if ($count == $section_key)
                            {
                                //this is the one
                                $text = $section;
                            }
                        }
                    }
                }
            }

            if (isset($translations))
            {
                foreach ($translations as $key => $translation)
                {
                    if (substr($key, 0, 1) != ':')
                    {
                        $translations[':'.$key] = $translation;
                        unset($translations[$key]);
                    }
                }
            }

            //apply translations and replacements
            if (isset($translations)) $text = strtr($text, $translations);
            if (isset($count)) $text = str_replace(':count', $count, $text);
        }

        return html_entity_decode($text);
    }
}
