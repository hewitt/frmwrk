<?php

// **PREVENTING SESSION HIJACKING**
// Prevents javascript XSS attacks aimed to steal the session ID
ini_set('session.cookie_httponly', 1);

// Adds entropy into the randomization of the session ID, as PHP's random number
// generator has some known flaws
ini_set('session.entropy_file', '/dev/urandom');

// Uses a strong hash
ini_set('session.hash_function', 'whirlpool');

//maximum lifetime of a session
if (defined('LOGIN_FREQUENCY')) $gc_maxlifetime = (strtotime(LOGIN_FREQUENCY) - time());
else $gc_maxlifetime = (strtotime('1 year') - time());
ini_set('session.gc_maxlifetime', $gc_maxlifetime);
session_set_cookie_params($gc_maxlifetime);

//give the session a unique name so it doesn't fall for common attacks or problems
session_name('session_id_frmwrk_' . $_ENV['ENVIRONMENT']);

// Probability of garbage collecting after starting session is equal to 1/gc_divisor
// Set this higher to reduce database calls per page load (it's not vital that old sessions be immediately removed, as they're expired, anyway)
ini_set('session.gc_divisor', '5');

// **PREVENTING SESSION FIXATION**
// Session ID cannot be passed through URLs
if (ini_set('session.use_only_cookies', 1) === false)
{
    echo "<h1>CRITICAL SECURITY ISSUE</h1>";
    echo "session.use_only_cookies is set to 0";
    exit(1);
}

// Uses a secure connection (HTTPS) if possible
//ini_set('session.cookie_secure', 1);

class db_session_handler extends Src\Core\Database implements SessionHandlerInterface
{
    public function __construct()
    {
        parent::__construct();
    }

    public function open ($save_path, $session_name): bool
    {
        return true;
    }

    public function close (): bool
    {
        return true;
    }

    public function read ($session_id): string
    {
        if (!isset($session_id)) $session_id='';

        $sql = "SELECT data FROM user_sessions WHERE id = :id";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':id', $session_id);
        $stmt->execute();
        $res = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if (count($res) === 1) return $res[0]['data'];

        return '';
    }

    public function write ($session_id, $session_data): bool
    {
        //get some common data points
        $ip_address = get_ip();
        $user_id = isset($_SESSION['user_id']) ? intval($_SESSION['user_id']) : null;
        $user_agent = $_SERVER['HTTP_USER_AGENT'];
        $current_time = time();

        //protect against common junk 'users' (typically bots)
        $user_agents_to_deny =
        [
            'ELB-HealthChecker/2.0'
        ];
        if (in_array($user_agent, $user_agents_to_deny)) return true;

        /*
         * We use this to check and see if the session record needs to be updated or
         * inserted, because if there is no filled in session data in previously
         * existing session records, then it's a bad session
         */
        $sql = "SELECT data FROM user_sessions WHERE id = :session_id";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':session_id', $session_id);
        $stmt->execute();
        $res = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if (empty($res))
        {
            if (count($res) === 0)
            {
                $sql =
                "
                    INSERT INTO user_sessions
                    (
                        id,
                        ip_address,
                        user_agent,
                        user_id,
                        data,
                        date_created,
                        last_activity
                    )
                    VALUES
                    (
                        :id,
                        :ip_address,
                        :user_agent,
                        :user_id,
                        :data,
                        :current_time,
                        :current_time
                    )
                ";

                $stmt = $this->db->prepare($sql);
                $stmt->bindParam(':id', $session_id, PDO::PARAM_STR);
                $stmt->bindParam(':ip_address', $ip_address, PDO::PARAM_STR);
                $stmt->bindParam(':user_agent', $_SERVER['HTTP_USER_AGENT'], PDO::PARAM_STR);
                $stmt->bindParam(':user_id', $user_id, PDO::PARAM_INT);
                $stmt->bindParam(':current_time', $current_time, PDO::PARAM_INT);
                $stmt->bindParam(':data', $session_data, PDO::PARAM_STR);

                if ($stmt->execute()) return true;
            }
        }
        else
        {
            if (!isset($_SERVER['REDIRECT_URL'])) $location = 'Unknown';
            else $location = $_SERVER['REDIRECT_URL'];

            $sql =
            "
                UPDATE user_sessions
                SET
                    ip_address = :ip_address,
                    user_agent = :user_agent,
                    user_id = :user_id,
                    data = :data,
                    last_activity = :current_time
                WHERE
                    id = :id
            ";

            $stmt = $this->db->prepare($sql);
            $stmt->bindParam(':ip_address', $ip_address, PDO::PARAM_STR);
            $stmt->bindParam(':user_agent', $_SERVER['HTTP_USER_AGENT'], PDO::PARAM_STR);
            $stmt->bindParam(':user_id', $user_id, PDO::PARAM_INT);
            $stmt->bindParam(':data', $session_data, PDO::PARAM_STR);
            $stmt->bindParam(':current_time', $current_time, PDO::PARAM_INT);
            $stmt->bindParam(':id', $session_id, PDO::PARAM_STR);

            if ($stmt->execute()) return true;
        }

        return false;
    }

    public function destroy ($session_id): bool
    {
        $sql = "DELETE FROM user_sessions WHERE id = :session_id";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':session_id', $session_id);
        if ($stmt->execute()) return true;

        return false;
    }

    public function gc (int $max_lifetime): int
    {
        //delete sessions of users not logged in after 5 minutes
        $sql = "DELETE FROM user_sessions WHERE user_id IS NULL AND last_activity < " . (time() - 300);
        $stmt = $this->db->prepare($sql);
        $stmt->execute();

        //delete sessions of users logged in after a period of time (if they haven't logged in, force them to relogin)
        $sql = "DELETE FROM user_sessions WHERE last_activity < " . (time() - $max_lifetime);
        $stmt = $this->db->prepare($sql);
        if ($stmt->execute()) return 1;

        return 0;
    }
}

//user session handler
use Src\Core\Database;
$session_db = new Database();

$db_session_handler = new db_session_handler($session_db->db);
session_set_save_handler($db_session_handler, true);
if (!session_id() and session_start() === false)
{
    echo "Couldn't start session. Please come back in a few hours, and try again. Let us know if this problem persists.";
    exit(1); //a response code other than 0 is a failure
}
$session_db = null;
