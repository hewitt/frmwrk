<?php

/*
|--------------------------------------------------------------------------
| Error reporting
| Useful to show every little problem during development, but only show hard errors in production
|--------------------------------------------------------------------------
|
*/
error_reporting(E_ALL);
ini_set('display_errors', 1);



/*
|--------------------------------------------------------------------------
| Define application settings
| Set variables in the config array, and then define them all as constants
|--------------------------------------------------------------------------
|
*/
define('MAINTENANCE_MODE', false);
define('DEFAULT_LANGUAGE', 'en-us');
define('SEND_TRANSACTION_EMAILS', true);
define('LOGIN_FREQUENCY', '1 year');
define('CONTACT_NAME', 'Mark Hewitt');
define('CONTACT_EMAIL', 'm@mh1.co');
define('SITE_VERSION', substr(md5(filemtime(SRC . '/Core/Config.php')), 0, 8));
define('SITE_UPDATED', filemtime(SRC . '/Core/Config.php'));



/*
|--------------------------------------------------------------------------
| Date Formats
|--------------------------------------------------------------------------
|
| We need to have a standard for date formatting, as we're an international
| company. We should always abide by the ISO-8601 standard. This makes it
| a lot easier to utilize that standard without having to write it out.
|
*/
define('TIME_FORMAT', 'Y-m-d H:i:s T');
define('TIME_FORMAT_DATE', 'Y-m-d');



/*
|--------------------------------------------------------------------------
| Timezone
|--------------------------------------------------------------------------
|
| I'm on the East Coast, so times for me should all be in EST/EDT, but some
| peope live in other places. We'll set the timezone for them when we
| initialize their user object.
|
*/
date_default_timezone_set('America/New_York');



/*
|--------------------------------------------------------------------------
| Environment Variables File
|--------------------------------------------------------------------------
|
| To keep secrets a bit more secure, we put them into a special file so
| people can't see it, but the server can. Any secrets that we must use on
| the site should be stored here. The file is listed in .gitignore to
| prevent the secrets from making their way up to source control.
|
*/
if (is_readable(ROOT . '/.env'))
{
    $lines = file(ROOT . '/.env', FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
    foreach ($lines as $line)
    {
        if (str_starts_with(trim($line), '#')) continue;

        list($name, $value) = explode('=', $line, 2);
        $name = trim($name);
        $value = trim($value);

        if (!array_key_exists($name, $_SERVER) and !array_key_exists($name, $_ENV))
        {
            putenv(sprintf('%s=%s', $name, $value));
            $_ENV[$name] = $value;
            $_SERVER[$name] = $value;
        }
    }
}
