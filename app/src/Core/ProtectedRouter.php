<?php

namespace Src\Core;

use Src\Core\Route;

class ProtectedRouter extends \Steampixel\Route
{
    /*
    |--------------------------------------------------------------------------
    | Settings and variables
    |--------------------------------------------------------------------------
    |
    | Change these values with the helper methods below, so they aren't
    | hardcoded in this file. Having setting updates separate enables easy and
    | quick updating of this file. Abstraction is good.
    |
    */
    protected static $accessDenied = null; //permission denied

    /*
    |--------------------------------------------------------------------------
    | Helper Functions
    |--------------------------------------------------------------------------
    |
    | Set class settings so they aren't hardcoded into this file
    |
    | @param string $value      Value that the variable will be changed to
    |
    */
    public static function accessDenied($value): void { self::$accessDenied = $value; }

    /*
    |--------------------------------------------------------------------------
    | Main Methods
    |--------------------------------------------------------------------------
    |
    | There are three main functions: init, filter, and run. Add a function
    | below to overwrite the base function located in \Steampixel\Route.
    |
    */

    // Filter the matching results from one/many to just one
    public static function filter($matching_routes): void
    {
        global $user;

        $public_routes = [];
        $private_routes = [];
        $accessAllowed = true;

        foreach ($matching_routes as $matching_route)
        {
            if (isset($matching_route['arguments']))
            {
                $arguments = $matching_route['arguments'];

                if (isset($arguments['auth']) and !empty($arguments['auth']))
                {
                    if ($arguments['auth'] === 'verified' and $user->is_logged_in())
                    {
                        $private_routes[] = $matching_route;
                        $accessAllowed = true;
                    }
                    else
                    {
                        $accessAllowed = false;
                    }
                }
                else
                {
                    $public_routes[] = $matching_route;
                    $accessAllowed = true;
                }
            }
        }

        $matching_routes = array_merge($private_routes, $public_routes);

        if (count($matching_routes))
        {
            static::run($matching_routes[0]);
        }
        else
        {
            if ($accessAllowed === false) static::run(static::$accessDenied);
            else static::run(static::$pathNotFound);
        }
    }
}
