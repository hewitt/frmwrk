<?php

/**
 * Class Users
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */

namespace Src\Model;

use Src\Core\Database,
    PDO;

class Users extends Database
{
    public $user_id;
    public $email;
    public $language = 'en-us';
    public $date_registered;

    function __construct()
    {
        parent::__construct();
    }

    public function getByID ($user_id = null): bool
    {
        $this->user_id = $user_id ?? $this->user_id;

        $sql = "SELECT user_id,email,language,date_registered FROM users WHERE user_id = :user_id";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam('user_id', $this->user_id, PDO::PARAM_STR);
        $stmt->execute();

        if ($stmt->rowCount())
        {
            $user = $stmt->fetch(PDO::FETCH_OBJ);
            foreach ($user as $column => $value) $this->{$column} = $value;
            return true;
        }

        return false;
    }

    public function getByEmail (string $email = null): bool
    {
        $this->email = $email ?? $this->email;

        $sql = "SELECT user_id,email,language,date_registered FROM users WHERE email = :email";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam('email', $email, PDO::PARAM_STR);
        $stmt->execute();

        if ($stmt->rowCount())
        {
            $user = $stmt->fetch(PDO::FETCH_OBJ);
            foreach ($user as $column => $value) $this->{$column} = $value;
            return true;
        }

        return false;
    }

    public function is_logged_in (): bool
    {
        return isset($_SESSION['user_id']);
    }

    public function login ($email, $password): bool
    {
        if ($this->is_logged_in()) return true;

        $email = strtolower(processString($email));
        $password = password_hash($password, PASSWORD_BCRYPT);

        $sql = "SELECT user_id,email,password,language,date_registered FROM users WHERE LOWER(email) = :email LIMIT 1";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam("email", $email, PDO::PARAM_STR);
        $stmt->execute();

        if ($stmt->rowCount())
        {
            $alleged_user = $stmt->fetch(PDO::FETCH_OBJ);

            if (password_verify($_POST['password'], $alleged_user->password))
            {
                foreach ($alleged_user as $column => $value)
                {
                    if ($column != 'password') $this->{$column} = $value;
                }

                //set session variable so we can pass the user between pages
                $_SESSION['user_id'] = $alleged_user->user_id;

                //log it
                log_info('Logged in');

                unset($alleged_user);
                return true;
            }
        }

        unset($alleged_user);
        return false;
    }

    public function register (string $email, string $password): bool
    {
        $this->email = strtolower(processString($email));
        $password = password_hash($password, PASSWORD_BCRYPT);

        //does user already exist?
        $sql = "SELECT user_id FROM users WHERE email = :email";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam("email", $email, PDO::PARAM_STR);
        $stmt->execute();

        if ($stmt->rowCount() === 0)
        {
            $this->date_registered = time();

            //insert user
            $sql = "INSERT INTO users (email, password, date_registered) VALUES (:email, :password, :date_registered)";
            $stmt = $this->db->prepare($sql);
            $stmt->bindParam("email", $this->email, PDO::PARAM_STR);
            $stmt->bindParam("password", $password, PDO::PARAM_STR);
            $stmt->bindParam("date_registered", $this->date_registered, PDO::PARAM_STR);
            if ($stmt->execute())
            {
                $this->user_id = $this->db->lastInsertId();

                //log it
                log_info('Registered user: ' . $email);

                return true;
            }
            else
            {
                //log it
                log_warning("Couldn't register user: " . $email);
            }
        }
        else
        {
            //log it
            log_notice('Attempted registration with existing email: ' . $email);
        }

        return false;
    }

    public function setRef (): string
    {
        $ref = substr(md5(microtime()),rand(0,26),32);
        $query = "SELECT hash FROM user_reset_requests WHERE hash = :ref";
        $stmt = $this->db->prepare($query);
        $stmt->bindParam(':ref', $ref, PDO::PARAM_STR);
        $stmt->execute();

        if ($stmt->rowCount() !== 0)
        {
            //there's already a ref with this hash, so we need to fire this again to get a different hash, hopefully without crashing into each other
            $this->setRef();
        }
        else
        {
            //delete previous requests for this user, so we don't spam our own database, and so any old requests can't be used by someone else
            $this->deleteRef($this->user_id);

            //now that the user doesn't have any records in the table, let's insert one
            $query = "INSERT INTO user_reset_requests (user_id, hash, date_requested) VALUES (:user_id, :ref, :date_requested)";
            $stmt = $this->db->prepare($query);
            $stmt->bindParam(':user_id', $this->user_id, PDO::PARAM_STR);
            $stmt->bindParam(':ref', $ref, PDO::PARAM_STR);
            $date_requested = time();
            $stmt->bindParam(':date_requested', $date_requested, PDO::PARAM_STR);
            $stmt->execute();

            if ($stmt->rowCount())
            {
                //log it
                log_info('Password reset requested: ' . $ref);

                return $ref;
            }
        }

        //if we get here, something went wrong
        log_warning("Couldn't set password reset request hash: " . $ref);

        return "";
    }

    public function verifyRef (string $ref): int
    {
        //get the user_id of this hash
        $sql = "SELECT user_id FROM user_reset_requests WHERE hash = :ref";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':ref', $ref, PDO::PARAM_STR);
        $stmt->execute();

        if ($stmt->rowCount())
        {
            $verified_user = $stmt->fetch(PDO::FETCH_OBJ);

            return $verified_user->user_id;
        }

        return 0;
    }

    public function deleteRef ($user_id): bool
    {
        $query = "DELETE FROM user_reset_requests WHERE user_id = :user_id";
        $stmt = $this->db->prepare($query);
        $stmt->bindParam(':user_id', $user_id, PDO::PARAM_STR);
        if ($stmt->execute()) return true;

        return false;
    }

    public function changePassword (string $new_password): bool
    {
        $new_password = password_hash($new_password, PASSWORD_BCRYPT);

        $sql = "UPDATE users SET password = :password WHERE user_id = :user_id";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':password', $new_password, PDO::PARAM_STR);
        $stmt->bindParam(':user_id', $this->user_id, PDO::PARAM_INT);
        if ($stmt->execute())
        {
            $this->deleteRef($this->user_id);

            //log it
            log_info('Changed password');

            return true;
        }

        //if we get here, something went wrong
        log_warning("Error while attempting to change password");

        return false;
    }

    public function deleteUser (): bool
    {
        $sql = "UPDATE users SET email = null, password = null, language = 'en-us' WHERE user_id = :user_id";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':user_id', $this->user_id, PDO::PARAM_INT);
        if ($stmt->execute())
        {
            //clear out password reset requests
            $this->deleteRef($this->user_id);

            //delete login sessions
            $sql = "DELETE FROM user_sessions WHERE user_id = :user_id";
            $stmt = $this->db->prepare($sql);
            $stmt->bindParam(':user_id', $this->user_id, PDO::PARAM_INT);
            $stmt->execute();

            //clear out user details in this user object
            $this->email = null;
            $this->language = 'en-us';

            //log it
            log_info('Account deleted');

            return true;
        }

        //if we get here, something went wrong
        log_warning("Error while attempting to delete account");

        return false;
    }
}
