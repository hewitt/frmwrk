<?php

namespace Src\Model;

use Src\Core\Database,
    PDO;

class Logs extends Database
{
    function __construct ()
    {
        parent::__construct();
    }

    public function emergency (string $message): bool { return $this->insert(0, $message); }
    public function alert (string $message): bool { return $this->insert(1, $message); }
    public function critical (string $message): bool { return $this->insert(2, $message); }
    public function error (string $message): bool { return $this->insert(3, $message); }
    public function warning (string $message): bool { return $this->insert(4, $message); }
    public function notice (string $message): bool { return $this->insert(5, $message); }
    public function info (string $message): bool { return $this->insert(6, $message); }
    public function debug (string $message): bool { return $this->insert(7, $message); }

    public function insert (int $level, string $message)
    {
        //if there's no message, then what are we even logging?
        if (empty($message)) return false;

        //if there's no level, assume it's a 6, but that should be impossible
        if (empty($level)) $level = 6;

        global $user, $current_route;

        //get the rest of the variables that are the same across all log levels
        $date_of_action = microtime(true);
        $user_id = ($user->user_id ?? 'null');
        $route = json_encode($current_route);

        //insert into the database
        $query = "INSERT INTO logs (level, date_of_action, user_id, message, route) VALUES (:level, :date_of_action, :user_id, :message, :route)";
        $stmt = $this->db->prepare($query);
        $stmt->bindParam("level", $level, PDO::PARAM_INT);
        $stmt->bindParam("date_of_action", $date_of_action, PDO::PARAM_STR);
        $stmt->bindParam("user_id", $user_id, PDO::PARAM_INT);
        $stmt->bindParam("message", $message, PDO::PARAM_STR);
        $stmt->bindParam("route", $route, PDO::PARAM_STR);
        if ($stmt->execute()) return true;

        return false;
    }

    //Example usage:
    //$this->log->write(['user_id' => 1, 'action'=>'Changed email to `something@new.com`']);
    public function write ($params = ['user_id' => null, 'action' => null, 'page' => null, 'level' => null])
    {
        if (isset($params['level']) AND !is_null($params['level']) AND (is_string($params['level']) OR $params['level'] < 0 OR $params['level'] < 7))
        {
            switch ($params['level'])
            {
                case 'emergency': // System is unusable.
                    $level = 0;
                    break;
                case 'alert': // Action must be taken immediately. Example: Entire website down, database unavailable, etc. This should trigger the SMS alerts and wake you up.
                    $level = 1;
                    break;
                case 'critical': // Critical conditions. Example: Application component unavailable, unexpected exception.
                    $level = 2;
                    break;
                case 'error': // Runtime errors that do not require immediate action but should typically be logged and monitored.
                    $level = 3;
                    break;
                case 'warning': // Exceptional occurrences that are not errors. Example: Use of deprecated APIs, poor use of an API, undesirable things that are not necessarily wrong.
                    $level = 4;
                    break;
                case 'notice': // Normal but significant events.
                    $level = 5;
                    break;
                case 'info': // Interesting events. Example: User logs in, SQL logs.
                    $level = 6;
                    break;
                case 'debug': // Detailed debug information.
                    $level = 7;
                    break;
                default:
                    $level = 6;
            }
        }
        else
        {
            $level = 6;
        }

        $date_of_action = microtime(true);
        $params['level'] = ((isset($params['level']) AND !is_null($params['level'])) ? $params['level'] : $this->level);
        $params['user_id'] = ((isset($params['user_id']) AND !is_null($params['user_id'])) ? $params['user_id'] : $this->user_id);
        $params['page'] = ((isset($params['page']) AND !is_null($params['page'])) ? $params['page'] : $this->page);
        $params['page'] = str_replace('//', '/', $params['page']);

        if ($level < 4 OR strtolower($this->user_type) !== 't')
        {
            $query = "INSERT INTO logs (level, date_of_action, user_id, action, page) VALUES (:level, :date_of_action, :user_id, :action, :page)";
            $stmt = $this->db->prepare($query);
            $stmt->bindParam("level", $level, PDO::PARAM_STR);
            $stmt->bindParam("date_of_action", $date_of_action, PDO::PARAM_STR);
            $stmt->bindParam("user_id", $params['user_id'], PDO::PARAM_STR);
            $stmt->bindParam("action", $params['action'], PDO::PARAM_STR);
            $stmt->bindParam("page", $params['page'], PDO::PARAM_STR);
            $stmt->execute();

            if ($stmt->rowCount() === 1)
            {
                //yes
                return true;
            }
            else
            {
                //no
                return false;
            }
        }
    }
}
