<?php

use Src\Core\ProtectedRouter as Route;

//homepage
Route::get('/', function(){ redirect('/login'); });
Route::get('/', 'Src\Controller\HomeController::index', ['auth'=>'verified']);

//login
Route::get('/login', function(){ redirect(); }, ['auth'=>'verified']);
Route::get('/login', 'Src\Controller\AccountController::login');
Route::post('/login', 'Src\Controller\AccountController::processLogin');

//register
Route::get('/register', 'Src\Controller\AccountController::register');
Route::post('/register', 'Src\Controller\AccountController::processRegistration');
Route::get('/register', function(){ redirect(); }, ['auth'=>'verified']);

//account
Route::get('/account', 'Src\Controller\AccountController::index', ['auth'=>'verified']);

//account/delete
Route::get('/account/delete', 'Src\Controller\AccountController::deleteAccount', ['auth'=>'verified']);
Route::post('/account/delete', 'Src\Controller\AccountController::confirmDeleteAccount', ['auth'=>'verified']);

//reset
Route::get('/reset', 'Src\Controller\AccountController::passwordResetForm', ['auth'=>'verified']);
Route::post('/reset', 'Src\Controller\AccountController::processPasswordResetForm', ['auth'=>'verified']);
Route::get('/reset', 'Src\Controller\AccountController::requestPasswordReset');
Route::post('/reset', 'Src\Controller\AccountController::processRequestPasswordReset');

//reset/______
Route::get('/reset/(.*)', 'Src\Controller\AccountController::passwordResetForm');
Route::post('/reset/(.*)', 'Src\Controller\AccountController::processPasswordResetForm');

//terms
Route::get('/terms', 'Src\Controller\AccountController::termsAndConditions');

//privacy
Route::get('/privacy', 'Src\Controller\AccountController::privacyPolicy');

//logout
Route::get('/logout', 'Src\Controller\AccountController::logout');
