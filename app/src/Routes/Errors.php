<?php

use Src\Core\ProtectedRouter as Route;

// Define which pages to present when an error occurs
Route::pathNotFound('Src\Controller\ErrorController::code404');
Route::methodNotAllowed('Src\Controller\ErrorController::code405');
Route::accessDenied('Src\Controller\ErrorController::code401');
