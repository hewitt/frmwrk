<?php

/**
 * Class Error
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */

namespace Src\Controller;

class ErrorController
{
    /**
     * PAGE: index
     * This method handles the error page that will be shown when a page is not found
     */
    public static function index()
    {
        header("HTTP/1.0 404 Not Found");

        $page_title = '404 Error';
        require SRC . '/View/_layouts/header.php';
        require SRC . '/View/error/index.php';
        require SRC . '/View/_layouts/footer.php';
    }

    public static function code401()
    {
        header('HTTP/1.1 401 Unauthorized');
        require SRC . '/View/error/401.php';
    }

    public static function code403()
    {
        header('HTTP/1.1 403 Forbidden');
        require SRC . '/View/error/403.php';
    }

    public static function code404()
    {
        header('HTTP/1.1 404 Not Found');
        require SRC . '/View/error/404.php';
    }

    public static function code405()
    {
        header('HTTP/1.1 405 Method Not Allowed');
        require SRC . '/View/error/405.php';
    }

    public static function code500()
    {
        header('HTTP/1.1 500 Internal Server Error');
        require SRC . '/View/error/500.php';
    }

    public static function code502()
    {
        header('HTTP/1.1 502 Bad Gateway');
        require SRC . '/View/error/502.php';
    }

    public static function code503()
    {
        header('HTTP/1.1 503 Service Unavailable');
        require SRC . '/View/error/503.php';
    }
}
