<?php

/**
 * Class HomeController
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */

namespace Src\Controller;

use Src\Model\Logs;

class HomeController
{
    public static function index(): void
    {
        global $user;

        require SRC . '/View/_layouts/header.php';
        require SRC . '/View/_layouts/navigation.php';
        require SRC . '/View/home/index.php';
        require SRC . '/View/_layouts/footer.php';
    }
}
