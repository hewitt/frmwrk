<?php

/**
 * Class AccountController
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */

namespace Src\Controller;

use Src\Model\Logs,
    Src\Model\Users;

class AccountController
{
    public static function index(): void
    {
        global $user;

        $page_title = __('titles.account');
        require SRC . '/View/_layouts/header.php';
        require SRC . '/View/_layouts/navigation.php';
        require SRC . '/View/account/index.php';
        require SRC . '/View/_layouts/footer.php';
    }

    public static function termsAndConditions(): void
    {
        $page_title = __('titles.terms_and_conditions');
        require SRC . '/View/_layouts/header.php';
        require SRC . '/View/account/terms-conditions.php';
        require SRC . '/View/_layouts/footer.php';
    }

    public static function privacyPolicy(): void
    {
        $page_title = __('titles.privacy_policy');
        require SRC . '/View/_layouts/header.php';
        require SRC . '/View/account/privacy-policy.php';
        require SRC . '/View/_layouts/footer.php';
    }

    public static function register(): void
    {
        $page_title = __('titles.register');
        require SRC . '/View/_layouts/header.php';
        require SRC . '/View/account/register.php';
        require SRC . '/View/_layouts/footer.php';
    }

    public static function processRegistration(): void
    {
        global $user;

        $email = $_POST['email'];
        $password = $_POST['password'];

        //attempt to register user
        if ($user->register($email, $password) === true)
        //success
        {
            //login
            if ($user->login($email, $password))
            {
                //redirect to account page
                header('Location: ' . URL . '/account');
                exit;
            }
            else
            {
                //show the user an error message
                toast("We couldn't automatically log you in after registration. Something may be wrong with our systems. Please try to login manually. If you cannot, contact an adminstrator.", 'error');

                //redirect to login page
                header('Location: ' . URL . '/login');
                exit;
            }
        }
        //error
        else
        {
            //log registration error with email in the message
            info_warning('Registration error: ' . $email);

            //show the user an error message
            toast('Registration failed. Please try again later, or contact an administrator.', 'error');

            //redirect to registration page with error
            header('Location: ' . URL . '/register');
            exit;
        }
    }

    public static function login(): void
    {
        $page_title = __('titles.login');
        require SRC . '/View/_layouts/header.php';
        require SRC . '/View/account/login.php';
        require SRC . '/View/_layouts/footer.php';
    }

    public static function processLogin(): void
    {
        global $user,$current_route;

        $email = $_POST['email'];
        $password = $_POST['password'];

        if ($user->login($email, $password))
        {
            //success
            header('Location: '.URL);
            exit;
        }
        else
        {
            //log it
            log_info("Couldn't log in");

            //show the user an error message
            toast("Couldn't login. Please try again.", 'error');

            header('Location: '.URL.'/login');
            exit;
        }
    }

    public static function logout(): void
    {
        global $user;

        //log it
        log_info('Logged out');

        //save the old session ID to compare against the new one
        $original_session_id = session_id();

        //destroy old session, and create a new blank one
        session_unset();
        session_regenerate_id();
        session_destroy();

        //something went wrong with the logout, and it didn't delete the session
        //show the user an error message
        if ($original_session_id == session_id()) toast("Couldn't logout");

        //whether the logout worked or not, let's redirect to /login
        header('Location: '.URL.'/login');
        exit;
    }

    public static function requestPasswordReset(): void
    {
        $page_title = __('titles.reset_password');
        require SRC . '/View/_layouts/header.php';
        require SRC . '/View/account/request-password-reset.php';
        require SRC . '/View/_layouts/footer.php';
    }

    public static function processRequestPasswordReset(): void
    {
        global $user;

        //is email address set?
        if (isset($_POST['email']))
        {
            $email = $_POST['email'];

            if (filter_var($email, FILTER_VALIDATE_EMAIL))
            //yes
            {
                echo "<h1>Processing passsword reset request</h1>";

                //does email address match any user in the database?
                if ($user->getByEmail($email) !== false)
                {
                    //reset password to be unique (and random) string of characters
                    $ref = $user->setRef();
                    if (!empty($ref))
                    {
                        //email "ref" link to reset along with that string of characters
                        //link: /account/reset/5f453zst2
                        echo "<p>Sending email to {$email}: " . URL . "/reset/{$ref}</p>";

                        //log the email being sent
                        log_info("Password reset email sent to '{$email}'");
                    }
                }
            }
        }
    }

    public static function passwordResetForm(string $ref = null): void
    {
        global $user;

        //if the user isn't logged in, we need a hash to verify the user
        //if the hash isn't set, redirect to the request page
        //if the hash is set, but it's not valid, redirect to the request page
        if (!$user->is_logged_in() and (empty($ref) or $user->verifyRef($ref) === 0))
        {
            header('Location: ' . URL . '/reset');
            exit;
        }

        //either the user is logged in, or the hash is valid
        $page_title = __('titles.reset_password');
        require SRC . '/View/_layouts/header.php';
        if ($user->is_logged_in())
        {
            require SRC . '/View/_layouts/navigation.php';
        }
        require SRC . '/View/account/reset-password.php';
        require SRC . '/View/_layouts/footer.php';
    }

    public static function processPasswordResetForm(string $ref = null): void
    {
        global $user;

        //set the user variable based on the reference hash
        if (!empty($ref))
        {
            $verified_user_id = $user->verifyRef($ref);
            $user->getByID($verified_user_id);
        }

        //if the user has data in it, either from the hash or from the user being logged in, reset the password
        if (!empty($user->user_id))
        {
            //let's reset the password
            if ($user->changePassword($_POST['password']))
            {
                //if user isn't already logged in, log them in
                if (!$user->is_logged_in()) $user->login($user->email, $_POST['password']);

                header('Location: ' . URL . '/account');
                exit;
            }
        }

        //if we're here, something went wrong
        if (!empty($ref)) header('Location: ' . URL . '/reset/' . $ref);
        else header('Location: ' . URL . '/reset');
        exit;
    }

    public static function deleteAccount(): void
    {
        $page_title = __('titles.delete_account');
        require SRC . '/View/_layouts/header.php';
        require SRC . '/View/account/delete-account.php';
        require SRC . '/View/_layouts/footer.php';
    }

    public static function confirmDeleteAccount(): void
    {
        global $user;

        if ($_POST['email'] === $user->email)
        {
            if ($user->deleteUser())
            {
                //save the old session ID to compare against the new one
                $original_session_id = session_id();

                //destroy old session, and create a new blank one
                session_unset();
                session_regenerate_id();
                session_destroy();

                //something went wrong with the logout, and it didn't delete the session
                //show the user an error message
                if ($original_session_id == session_id()) toast("Couldn't logout");

                //whether the logout worked or not, let's redirect to /login
                header('Location: '.URL.'/login');
                exit;
            }
        }
        else
        {
            //show the user an error message
            toast('Incorrect email address', 'error');
        }

        //it must not have worked
        header('Location: ' . URL . '/account/delete');
        exit;
    }
}
