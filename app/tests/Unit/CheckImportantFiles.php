<?php declare(strict_types=1);
use PHPUnit\Framework\TestCase;

if (!defined('ROOT')) define('ROOT', dirname(dirname(__DIR__)));
if (!defined('SRC')) define('SRC', ROOT . '/src');

class CheckImportantFiles extends TestCase
{
    public function testCoreFiles()
    {
        $this->assertIsReadable(SRC.'/Core/Config.php');
        $this->assertIsReadable(SRC.'/Core/Database.php');
        $this->assertIsReadable(SRC.'/Core/Sessions.php');
        $this->assertIsReadable(SRC.'/Model/Users.php');
    }

    public function testRouterRelatedFiles()
    {
        $this->assertIsReadable(ROOT.'/public/.htaccess');
        $this->assertIsReadable(ROOT.'/public/index.php');
        $this->assertIsReadable(SRC.'/Core/Router.php');
        $this->assertIsReadable(SRC.'/Routes/General.php');
        $this->assertIsReadable(SRC.'/Controller/HomeController.php');
    }

    public function testUserRelatedFiles()
    {
        $this->assertIsReadable(SRC.'/Core/Sessions.php');
        $this->assertIsReadable(SRC.'/Model/Users.php');
    }

    public function testLocalizationRelatedFiles()
    {
        $this->assertIsReadable(SRC.'/Core/Localization.php');
        $this->assertIsReadable(SRC.'/Functions/Localization.php');
        $this->assertDirectoryExists(SRC.'/Lang');
        $this->assertDirectoryExists(SRC.'/Lang/en-us');
    }
}
