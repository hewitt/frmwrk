<?php declare(strict_types=1);
use PHPUnit\Framework\TestCase;

if (!defined('ROOT')) define('ROOT', dirname(dirname(__DIR__)));
if (!defined('SRC')) define('SRC', ROOT . '/src');

require SRC . '/Core/Config.php';

class CheckConfig extends TestCase
{
    public function testConfigObject()
    {
        $this->assertIsString(DEFAULT_LANGUAGE);
        $this->assertIsString(LOGIN_FREQUENCY);
        $this->assertIsString(TIME_FORMAT);
        $this->assertIsString(TIME_FORMAT_DATE);
    }
}
