<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class PermissionsInstallation extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up(): void
    {
        $permissions = $this->table('permissions', ['id' => false, 'primary_key' => 'permission_id']);
        $permissions->addColumn('permission_id', 'integer', ['limit' => 11, 'signed' => false, 'identity' => true])
                    ->addColumn('permission', 'string', ['limit' => 255, 'collation' => 'utf8mb4_unicode_ci'])
                    ->create();

        $roles = $this->table('roles', ['id' => false, 'primary_key' => 'role_id']);
        $roles->addColumn('role_id', 'integer', ['limit' => 11, 'signed' => false, 'identity' => true])
              ->addColumn('name', 'string', ['limit' => 255, 'collation' => 'utf8mb4_unicode_ci'])
              ->addColumn('description', 'text', ['null' => true, 'collation' => 'utf8mb4_unicode_ci'])
              ->create();

        $roles_permissions = $this->table('roles_permissions', ['id' => false, 'primary_key' => ['role_id', 'permission_id']]);
        $roles_permissions->addColumn('role_id', 'integer', ['limit' => 11, 'signed' => false])
                          ->addColumn('permission_id', 'integer', ['limit' => 11, 'signed' => false])
                          ->addColumn('value', 'string', ['limit' => 32, 'collation' => 'utf8mb4_unicode_ci'])
                          ->create();

        $user_permissions = $this->table('user_permissions', ['id' => false, 'primary_key' => ['user_id', 'permission_id']]);
        $user_permissions->addColumn('user_id', 'integer', ['limit' => 11, 'signed' => false])
                         ->addColumn('permission_id', 'integer', ['limit' => 11, 'signed' => false])
                         ->addColumn('value', 'boolean', ['limit' => 1, 'signed' => false, 'default' => 1])
                         ->addColumn('date_added', 'integer', ['limit' => 10, 'signed' => false]) //unsigned because we aren't adding records from the past
                         ->create();

        $user_roles = $this->table('user_roles', ['id' => false, 'primary_key' => ['user_id', 'role_id']]);
        $user_roles->addColumn('user_id', 'integer', ['limit' => 11, 'signed' => false])
                   ->addColumn('role_id', 'integer', ['limit' => 11, 'signed' => false])
                   ->addColumn('date_added', 'integer', ['limit' => 10, 'signed' => false]) //unsigned because we aren't adding records from the past
                   ->create();
    }

    public function down(): void
    {
        $this->table('permissions')->drop()->save();
        $this->table('roles')->drop()->save();
        $this->table('roles_permissions')->drop()->save();
        $this->table('user_permissions')->drop()->save();
        $this->table('user_roles')->drop()->save();
    }
}
