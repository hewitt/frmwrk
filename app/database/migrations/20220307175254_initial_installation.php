<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class InitialInstallation extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up(): void
    {
        $logs = $this->table('logs', ['id' => false, 'primary_key' => 'id']);
        $logs->addColumn('id', 'integer', ['limit' => 11, 'signed' => false, 'identity' => true])
             ->addColumn('level', 'boolean', ['limit' => 1, 'signed' => false, 'default' => 6])
             ->addColumn('date_of_action', 'decimal', ['precision' => 14, 'scale' => 4])
             ->addColumn('user_id', 'integer', ['limit' => 11, 'signed' => false, 'null' => true])
             ->addColumn('message', 'string', ['limit' => 255, 'collation' => 'utf8mb4_unicode_ci', 'null' => true])
             ->addColumn('route', 'json', ['null' => true])
             ->create();

        $users = $this->table('users', ['id' => false, 'primary_key' => 'user_id']);
        $users->addColumn('user_id', 'integer', ['limit' => 11, 'signed' => false, 'identity' => true])
              ->addColumn('email', 'string', ['limit' => 255, 'collation' => 'utf8mb4_unicode_ci', 'null' => true])
              ->addColumn('password', 'string', ['limit' => 60, 'collation' => 'utf8mb4_unicode_ci', 'null' => true])
              ->addColumn('language', 'string', ['limit' => 8, 'collation' => 'utf8mb4_unicode_ci', 'default' => 'en-us'])
              ->addColumn('date_registered', 'integer', ['limit' => 10, 'signed' => false, 'null' => true]) //unsigned because we aren't adding records from the past
              ->addIndex(['email'])
              ->create();

        $users = $this->table('user_reset_requests', ['id' => false, 'primary_key' => 'hash']);
        $users->addColumn('user_id', 'integer', ['limit' => 11, 'signed' => false])
              ->addColumn('hash', 'string', ['limit' => 32, 'collation' => 'utf8mb4_unicode_ci'])
              ->addColumn('date_requested', 'integer', ['limit' => 10, 'signed' => false]) //unsigned because we aren't adding records from the past
              ->addIndex(['user_id'])
              ->create();

        $user_sessions = $this->table('user_sessions', ['id' => false, 'primary_key' => 'id']);
        $user_sessions->addColumn('id', 'string', ['limit' => 191, 'collation' => 'utf8mb4_unicode_ci'])
                      ->addColumn('ip_address', 'string', ['limit' => 50, 'collation' => 'utf8mb4_unicode_ci', 'null' => true])
                      ->addColumn('user_agent', 'text', ['null' => true, 'collation' => 'utf8mb4_unicode_ci'])
                      ->addColumn('user_id', 'integer', ['limit' => 11, 'null' => true])
                      ->addColumn('data', 'text', ['null' => true, 'collation' => 'utf8mb4_unicode_ci'])
                      ->addColumn('date_created', 'integer', ['limit' => 10, 'signed' => false]) //unsigned because we aren't adding records from the past
                      ->addColumn('last_activity', 'integer', ['limit' => 10, 'signed' => false]) //unsigned because we aren't worried about pre-1970
                      ->addIndex(['user_id'])
                      ->create();
    }

    public function down(): void
    {
        $this->table('logs')->drop()->save();
        $this->table('users')->drop()->save();
        $this->table('user_reset_requests')->drop()->save();
        $this->table('user_sessions')->drop()->save();
    }
}
