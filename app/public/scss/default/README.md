## DEFAULT

    @author Mark Hewitt
    @link https://gitlab.com/hewitt/
    @created 2020-08-04
    @updated 2022-01-22

## Overview

The files contained in this folder constitutes the entirety of the SMS GROUP NA standardized colors and styling rules for web applications. These SCSS files should be used as the basis for your project's GUI. Any colors not already listed in `_variables.scss` are unofficial, and are thus up to you. Any additions or edits to these files *should not be done in these files*. Any changes should be done in SCSS files specific to your project. When it comes time to update the default files with the latest colors, you won't want to lose the different colors you've added to the project, causing things to break upon compiling.

## Installation

Paste these files in your `/public/scss/` folder so that your structure looks like the following: `/public/scss/default/README.md`.

Import the package file (`all.scss`) into your primary SCSS file, or import the partials (`_breakpoints.scss` and `_variables.scss`) into other stylesheets by using the following code: `@import 'default/all';`

Compile the SCSS into CSS using your preferred SASS/SCSS compliler.

## Usage

Import files using the following code:
```
@import 'default/all';
```

Use breakpoints mixin by implementing the following code:
```
.logo {
    flex: 1 1 auto;
    font-size: 0;
    @include breakpoint(sm) {
        flex: 0;
    }
}
```
