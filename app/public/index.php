<?php

declare(strict_types = 1);

namespace Src;



/*
|--------------------------------------------------------------------------
| Website Constants
|--------------------------------------------------------------------------
|
| To provide quick links to frequently referenced parts of the site, we can
| define constants to refernce those parts without having to be verbose.
|
*/
define('ROOT', str_replace('\\', '/', dirname(__DIR__)));
define('SRC', ROOT . '/src');
if (
    (!empty($_SERVER['HTTP_X_FORWARDED_PROTO']) and $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https')
    or (!empty($_SERVER['REQUEST_SCHEME']) and $_SERVER['REQUEST_SCHEME'] == 'https')
    or (!empty($_SERVER['HTTPS']) and $_SERVER['HTTPS'] == 'on')
    or (!empty($_SERVER['SERVER_PORT']) and $_SERVER['SERVER_PORT'] == '443')
) define('URL_PROTOCOL', 'https://');
else define('URL_PROTOCOL', 'http://');
define('URL_PUBLIC_FOLDER', 'public');
define('URL_DOMAIN', $_SERVER['HTTP_HOST']);
define('URL_SUB_FOLDER', str_replace(URL_PUBLIC_FOLDER, '', str_replace("\\", '/', dirname($_SERVER['SCRIPT_NAME']))));
define('URL', rtrim(URL_PROTOCOL . URL_DOMAIN . URL_SUB_FOLDER, '/'));



/*
|--------------------------------------------------------------------------
| Configuration Files
|--------------------------------------------------------------------------
|
| Include required files like autoload, functions, config, etc.
|
*/
// This is the auto-loader for Composer-dependencies (to load tools into your project).
require ROOT . '/vendor/autoload.php';

// load application config (error reporting etc.)
require SRC . '/Core/Config.php';



/*
|--------------------------------------------------------------------------
| Maintenance Mode
|--------------------------------------------------------------------------
|
| Sometimes the site needs to be put into maintenance mode while we test
| things. User #1 will still be able to access the site as normally, to
| test parts of the site while no one else can see them.
|
*/
if (defined('MAINTENANCE_MODE') and MAINTENANCE_MODE === true)
{
    echo "This website is currently undergoing maintenance. Please try again later.";
    exit;
}



/*
|--------------------------------------------------------------------------
| Function Files
|--------------------------------------------------------------------------
|
| Modularized function files allow us to organize functions used on the
| site. Separating functions also helps with loading only what we need.
|
*/
require SRC . '/Functions/General.php';
require SRC . '/Functions/ProcessVariables.php';
require SRC . '/Functions/Logs.php';
require SRC . '/Functions/Localization.php';



/*
|--------------------------------------------------------------------------
| User Object
|--------------------------------------------------------------------------
|
| We have the concept of a user because we attribute subscriptions to
| individual people. One person might want to watch something that another
| person doesn't, or two people might want to watch the same thing.
|
*/
require SRC . '/Core/Sessions.php';
use Src\Model\Users;
$user = new Users();
if (isset($_SESSION['user_id']))
{
    //get user details
    $user->getByID($_SESSION['user_id']);
}



/*
|--------------------------------------------------------------------------
| Localization
|--------------------------------------------------------------------------
|
| People speak different languages. This allows users to change the site's
| language for better understanding and customization.
|
*/
use Src\Core\Localization;
$localization = new Localization();

//change site's localization to the user's selected language
if (isset($user)) $localization->language = $user->language;



/*
|--------------------------------------------------------------------------
| Traffic Routing
|--------------------------------------------------------------------------
|
| Specify what users should see based on URLs requested. See documentation
| at "docs/Routing.md" for more information.
|
*/
require_once SRC . '/Core/Router.php';
use Src\Core\ProtectedRouter as Route;

//routes
$routers = array_diff(scandir(SRC . '/Routes'), ['.','..']);
foreach ($routers as $router) require_once SRC . '/Routes/' . $router;

//process the routes when the user loads the page
Route::init();
