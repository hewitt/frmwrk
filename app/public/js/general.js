//set cookie
function setCookie (name, value, expiration_hours)
{
    //parameter validation
    name = encodeURIComponent(name);
    value = encodeURIComponent(value);

    //calculate new date based on input
    var expiration_date = new Date();
    expiration_date.setHours(expiration_date.getHours() + expiration_hours);

    //write the cookie
    document.cookie = name + '=' + value + ((expiration_hours == null) ? '' : '; expires=' + expiration_date.toUTCString()) + '; path=/';
}

//get cookie information
function getCookie (name)
{
    name = name + '=';

    //get list of cookies for this domain, and break them up into individual cookie chunks
    var decodedCookie = decodeURIComponent(document.cookie);
    var cookies = decodedCookie.split(';');

    //loop through list of cookies
    for (var i = 0; i < cookies.length; i++)
    {
        //get the entire cookie string, and trim off whitespace
        var cookie = cookies[i].trim();

        //if this is the chosen cookie, return it
        if (cookie.indexOf(name) == 0) return cookie.substring(name.length, cookie.length);
    }

    //either there were no cookies, or none with the provided name
    return '';
}

String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};

function pa (message)
{
    var severity = 'log';

    if (severity == 'log') console.log(message)
    else if (severity == 'warn') console.warn(message)
    else if (severity == 'error') console.error(message)
}

function number_format (number, decimals, dec_point, thousands_sep)
{
    // Strip all characters but numerical ones.
    number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
    var n = !isFinite(+number) ? 0 : +number,
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
        dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
        s = '',
        toFixedFix = function (n, prec)
        {
            var k = Math.pow(10, prec);
            return '' + Math.round(n * k) / k;
        };
    // Fix for IE parseFloat(0.55).toFixed(0) = 0;
    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
    if (s[0].length > 3)
    {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
    }
    if ((s[1] || '').length < prec)
    {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1).join('0');
    }
    return s.join(dec);
}

$(function () {
    //when the toast is clicked, hide it, but don't remove it, so we can view it with debugging tool if we need to
    $(".toast").on('click', function(event) {
        if (event.target.tagName.toLowerCase() !== 'a') event.preventDefault();
        $(".toast").fadeOut(200);
    });
});
